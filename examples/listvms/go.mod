module gitlab.com/kubeclouds/kubevirt-client-go/examples/listvms

go 1.12

replace (
	github.com/go-kit/kit => github.com/go-kit/kit v0.3.0
	github.com/openshift/api => github.com/openshift/api v0.0.0-20200526144822-34f54f12813a
    github.com/openshift/client-go => github.com/openshift/client-go v0.0.0-20200521150516-05eb9880269c
	github.com/operator-framework/operator-lifecycle-manager => github.com/operator-framework/operator-lifecycle-manager v0.0.0-20190128024246-5eb7ae5bdb7a

	k8s.io/client-go => k8s.io/client-go v0.19.3
	k8s.io/cluster-bootstrap => k8s.io/cluster-bootstrap v0.19.3
	gitlab.com/kubeclouds/kubevirt-client-go => ../../
)

require (
	github.com/spf13/pflag v1.0.5
	gitlab.com/kubeclouds/kubevirt-client-go v0.0.0-20201105175238-5f56cad93c59
	k8s.io/apimachinery v0.19.3
)
